﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Curency   //  в валюте много кошельков ( она одна)  , 
    //большое количество кошельков выражается через коллекцию объектов  
    {
        public int IdCurency { get; set; }
        public string NameCurency { get; set; }
        public ICollection<Purse> Purse { get; set; }
        public Curency()
        {
            Purse = new List<Purse>();
        }
    }

}