﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
   public class CategoryTransaction
    {
        public int IdCategory {get;set;}
        public string Type {get;set;}
        [Column("Operation Name")]
        public string OperationName {get;set;}
        public ICollection<Transaction> Transactions { get; set; }
        CategoryTransaction()
        {
            Transactions = new List<Transaction>();
        }


    }
}
