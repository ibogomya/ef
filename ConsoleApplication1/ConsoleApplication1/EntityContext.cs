﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ConsoleApplication1
{
    class EntityContext : DbContext
    {
        public EntityContext()
            :base("EntityContext")
        {
            this.Configuration.LazyLoadingEnabled = false ;
        }
        public DbSet<Transaction> Transactions {get;set;}
        public DbSet<Purse> Purse { get; set; }
        public DbSet<CategoryTransaction> CategoryTransactions { get; set; }
        public DbSet<Curency> Curency { get; set; }
        public DbSet<CurencyConversion> CurencyConversion { get; set; }
        public DbSet<Cash> Cash { get; set; } 
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryTransaction>().ToTable("CategoryTransactions")
                .HasKey(p => p.IdCategory)
                .HasMany(t => t.Transactions)
                .WithRequired(j => j.CategoryTransaction)
                .HasForeignKey(m => m.IdCategory);

            modelBuilder.Entity<Purse>().ToTable("Purse")
                .HasKey(d => d.IdPurse)
                .HasMany(g => g.Transactions)
                .WithRequired(q => q.Purse)
                .HasForeignKey(v => v.IdPurse);

            modelBuilder.Entity<Curency>().ToTable("Curency")
                .HasKey(c => c.IdCurency)
                .HasMany(e => e.Purse)
                .WithRequired(q => q.Curency)
                .HasForeignKey(k => k.IdCurency);

            modelBuilder.Entity<Transaction>().ToTable("Transactions")
                .HasKey(f => f.IdTransaction);

            modelBuilder.Entity<CurencyConversion>().ToTable("CurrencyConversion")
                .HasKey(v => v.IdCurencyConversion);

            modelBuilder.Entity<Cash>().ToTable("Cash")
                .HasKey(x => x.IdDate);

            base.OnModelCreating(modelBuilder);


        }
    }
}
