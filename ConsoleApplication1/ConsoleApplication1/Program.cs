﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (EntityContext db = new EntityContext())
            {
                decimal? runningTotalBlr = 0, runningTotalUsd = 0;
                var transaction = db.Cash
                    .Include("Purse")
                    .Include("Curency")
                    .GroupJoin
                    (
                      db.Transactions,
                      t => t.Date,
                      c => c.DateTimeTransaction,
                      (x, y) => new
                      {
                          Cashes = x,
                          Transaction = y
                      }
                    )
                    .SelectMany
                    (
                     z => z.Transaction.DefaultIfEmpty(),
                     (x, y) => new
                     {
                         BlrAmount = (decimal?)x.Transaction.Where(v => v.Purse.Curency.NameCurency == "Br").Sum(n => n.Amount),
                         UsdAmount = (decimal?)x.Transaction.Where(v => v.Purse.Curency.NameCurency == "Usd").Sum(v => v.Amount),
                         Date = x.Cashes.Date

                     }
                    )
                    .GroupBy(c => c.Date)
                    .ToList()
                    .Select(ch => new
                    {
                        blr = runningTotalBlr += ch.Sum(x => x.BlrAmount),
                        usd = runningTotalUsd += ch.Sum(x => x.UsdAmount),
                        Dates = ch.Key.Date
                    })
                    .GroupBy(x=>x.Dates)
                    .Select( cf => new
                        {
                       Blr = cf.Last().blr,
                       Usd = cf.Last().usd,
                       Dates = cf.Key.Date
                        }
                    );
 
                Console.WriteLine("BR --- USD --- Date");

                foreach (var t in transaction)
                    Console.WriteLine("{0} \t{1} \t{2}",t.Blr == null ? 0 : t.Blr, t.Usd == null ? 0 : t.Usd, t.Dates);
                    
                //    
                //    .Select(g=>
                //    {
                //        BlrAmount = g.Sum(g=>g.BlrAmount),
                //        UsdAmount = 0,
                //        Date = g.

                //    }
                //    )
                //    ;

                ////var transaction = db.Transactions
                ////    .Include(t => t.Purse.Curency)
                ////    .Include("Cash")
                ////    .Where(c => c.Purse.Curency.NameCurency == "Br" || c.Purse.Curency.NameCurency == "Usd")
                ////    .GroupBy(c => new { Date = c.DateTimeTransaction, Currency = c.Purse.Curency.NameCurency })
                ////    .Select(v => new { Date = v.Key.Date, Currency = v.Key.Currency, transactions = v.ToList() }).ToList();
                ////foreach (var t in transaction)
                ////    Console.WriteLine("{0} ----  {1} ------{2}", t.transactions.Sum(v => v.Amount), t.Currency, t.Date);

                    
                //    .GroupBy(t => t.DateTimeTransaction)
                //    .Select(y => new { a =  y.Key, b = y.ToList()}).ToList();
                //foreach (var t in transaction)
                //    Console.WriteLine("{0}--- {1}--- {2}---- {3}", t.IdTransaction, t.IdCategory, t.DateTimeTransaction, t.Purse);
         //    var cash =    db.Transactions.ToList();
         //    foreach (Transaction t in cash)
         //        Console.WriteLine("{0} --{1}", t.IdTransaction, t.DateTimeTransaction);
               // var dictionary = transaction.ToDictionary(x => x.Key);
                Console.ReadKey();
            }
        }
    }
}

