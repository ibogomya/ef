﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
   public class Transaction //много
    {
       public int IdTransaction { get; set; }
       public int IdPurse { get; set; }
       public DateTime DateTimeTransaction { get; set; }
       public int IdCategory { get; set; }
        public decimal Amount {get;set;}
        public Purse Purse { get; set; }
        public CategoryTransaction CategoryTransaction { get; set; }
    }
}
