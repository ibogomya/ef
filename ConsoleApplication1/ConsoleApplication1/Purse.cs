﻿using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
   public class Purse
    {
        public int IdPurse {get;set;}
        public decimal Balance {get;set;}
        public int IdCurency { get; set; }
        public string Account {get;set;}
        public Curency Curency { get; set; }   // при получении данных о кошельке, оно будет автоматически получать данные из бд
        public ICollection<Transaction> Transactions { get; set; }
        public Purse()
        {
            Transactions =  new List<Transaction>();
        } 
    }
}

